package exercises.ex1;

/**
 * The purpose of this test is to check that unauthorized user is not able to get pending agreements.
 *
 * 1. Send GET request without headers to https://api.ig.com/usermanagement/pendingagreements. Please use REST-assured (http://rest-assured.io) to do it.
 * 2. Check if:
 *    - response code is 401
 *    - global error message is: "error.security.client-token-missing"
 * 
 * You can additinally use these libraries: Junit, TestNg, AssertJ  
 */
public class RestCallTest {
}
